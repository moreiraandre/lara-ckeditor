# Notas
* É importante que a tag de script seja colocada antes de fechar a tag `body` e não dentro da tag `head`.
* O pacote npm está no `package.json`.
* Após a instalação eu copiei a pasta `ckeditor` dentro da pasta `node_modeles` para a pasta `public`.
